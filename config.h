/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>
#include <stddef.h>

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 1;
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "M+1CodeNerdFont-Bold:pixelsize=16:antialias=true:autohint=true" };
static const char dmenufont[]       =   "M+1CodeNerdFont-Bold:pixelsize=16:antialias=true:autohint=true";
static const char col_bg[]          = "#212121";
static const char col_sel_bg[]      = "#282a2e";
static const char col_fg[]          = "#109093";
static const char col_sel_fg[]      = "#ffffff"; //#379cd0";
static unsigned int baralpha        = 0xee;
static unsigned int borderalpha     = 0xff;

static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_fg,    col_bg,    col_bg},
	[SchemeSel]  = { col_sel_fg,col_sel_bg,col_sel_fg  },
};

/* tagging */
static const char* tags[] = {"一", "二", "三", "四", "五", "六", "七", "八", "九"};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "st",      NULL,     NULL,           0,         0,          1,           0,         -1 },
	{ "Vivaldi", NULL,     NULL,           0,         0,          0,           1,         -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,         -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

static const int lpm[] = {
        4, 3, 0
};

static const Layout layouts[] = {
	/* symbol     arrange function */
 	{ "󰨝 ",      dwindle },
	{ "",       tile },    /* first entry is default */
	{ " ",      NULL },    /* no layout function means floating behavior */
	{ "󰓩",       monocle },
	{ "󱪶 ",      bstackhoriz },
	{ "󱪷 ",      horizontal },
 	{ "󰕮 ",      spiral },
};


/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} }, \

/* #define TAGMON(KEY,TAG) \ */
/*     { MODKEY,                       KEY,      focusnthmon,    {.i  = TAG } }, \ */
/*     { MODKEY|ShiftMask,             KEY,      tagnthmon,      {.i  = TAG } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }


/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_bg, "-nf", col_fg, "-sb", col_sel_bg, "-sf", col_sel_fg, NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *slock[]  = { "slock", NULL };
static const char* prntscr[]  = { "/bin/sh", "-c", "maim -s | xclip -selection clipboard -t image/png", NULL };
static const char* music_pause[]  = { "cmus-remote", "-u", NULL };
static const char* music_next[]  = { "cmus-remote", "-n", NULL };
static const char* music_prev[]  = { "cmus-remote", "-r", NULL };
static const char* vol_up[]  = { "cmus-remote", "-v", "+10%", NULL };
static const char* vol_mute[]  = { "pulsemixer", "--toggle-mute", NULL };
static const char* vol_down[]  = { "cmus-remote", "-v", "-10%", NULL };
static const char* toggle_layout[]  = { "/bin/sh", "-c", "([[ $(xkblayout-state print %v) = 'dvorak' ]] && setxkbmap se -option caps:escape) || setxkbmap se dvorak -option caps:escape", NULL };

static const char* brightnessdown[] = { "/bin/sh", "-c", "light -U 10 && light -A 0.07", NULL };
static const char* brightnessup[] = { "light", "-A", "10", NULL };
static const char* calc[] = { "st", "-e", "qalc", NULL };
static const char* browser[] = { "librewolf-bin", NULL };
static const char* display[] = { "arandr", NULL };


static const Key keys[] = {
	/* modifier           key                      function        argument */
	{ MODKEY,             XK_e,                    spawn,          {.v = dmenucmd } },
	{ MODKEY,             XK_Return,               spawn,          {.v = termcmd } },
    { Mod4Mask|ShiftMask, XK_o,                    spawn,          {.v = prntscr } },
    { MODKEY|ShiftMask,   XK_d,                    spawn,          SHCMD("st -e xset dpms force off") },
    { Mod4Mask,           XK_m,                    spawn,          SHCMD("mumble rpc togglemute") },
    { Mod4Mask,           XK_v,                    spawn,          SHCMD("virt-manager") },
    { 0,                  XF86XK_AudioPlay,        spawn,          {.v = music_pause } },
    { 0,                  XF86XK_AudioNext,        spawn,          {.v = music_next } },
    { 0,                  XF86XK_AudioPrev,        spawn,          {.v = music_prev } },
    { 0,                  XF86XK_AudioLowerVolume, spawn,          {.v = vol_down } },
    { 0,                  XF86XK_AudioRaiseVolume, spawn,          {.v = vol_up } },
    { 0,                  XF86XK_AudioMute,        spawn,          {.v = vol_mute } },
    { MODKEY|ShiftMask,   XK_x,                    spawn,          {.v = slock } },
    { MODKEY|ShiftMask,   XK_Tab,                  spawn,          {.v = toggle_layout } },
    { 0,                  XF86XK_MonBrightnessUp,  spawn,          {.v = brightnessup } },
    { 0,                  XF86XK_MonBrightnessDown,spawn,          {.v = brightnessdown } },
    { 0,                  XF86XK_Calculator,       spawn,          {.v = calc } },
    { 0,                  XF86XK_HomePage,         spawn,          {.v = browser } },
    { 0,                  XF86XK_Search,           spawn,          {.v = dmenucmd } },
    { 0,                  XF86XK_Display,          spawn,          {.v = display } },

	{ MODKEY,             XK_b,                    togglebar,      {0} },
	{ MODKEY,             XK_n,                    focusstack,     {.i = +1 } },
	{ MODKEY,             XK_t,                    focusstack,     {.i = -1 } },
	{ MODKEY,             XK_i,                    incnmaster,     {.i = +1 } },
	{ MODKEY,             XK_d,                    incnmaster,     {.i = -1 } },
	{ MODKEY|ControlMask, XK_h,                    setmfact,       {.f = -0.01} },
	{ MODKEY|ControlMask, XK_l,                    setmfact,       {.f = +0.01} },
	{ MODKEY|ShiftMask,   XK_Return,               zoom,           {0} },
	{ MODKEY,             XK_Tab,                  view,           {0} },
	{ MODKEY|ShiftMask,   XK_q,                    killclient,     {0} },
	{ Mod4Mask,           XK_1,                    setlayout,      {.v = &layouts[0]} },
	{ Mod4Mask,           XK_2,                    setlayout,      {.v = &layouts[1]} },
	{ Mod4Mask,           XK_3,                    setlayout,      {.v = &layouts[2]} },
	{ Mod4Mask,           XK_4,                    setlayout,      {.v = &layouts[3]} },
	{ Mod4Mask,           XK_5,                    setlayout,      {.v = &layouts[4]} },
	{ Mod4Mask,           XK_6,                    setlayout,      {.v = &layouts[5]} },
	{ Mod4Mask,           XK_7,                    setlayout,      {.v = &layouts[6]} },
	{ MODKEY|ShiftMask,   XK_space,                togglefloating, {0} },
	{ MODKEY|ShiftMask,   XK_f,                    togglefullscr,  {0} },
	{ MODKEY,             XK_0,                    view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,   XK_0,                    tag,            {.ui = ~0 } },
	{ MODKEY,             XK_l,                    focusmon,       {.i = +1 } },
	{ MODKEY,             XK_h,                    focusmon,       {.i = -1 } },
	{ MODKEY|ShiftMask,   XK_l,                    tagmon,         {.i = +1 } },
	{ MODKEY|ShiftMask,   XK_h,                    tagmon,         {.i = -1 } },
	TAGKEYS(              XK_1,                             0)
	TAGKEYS(              XK_2,                             1)
	TAGKEYS(              XK_3,                             2)
	TAGKEYS(              XK_4,                             3)
	TAGKEYS(              XK_5,                             4)
	TAGKEYS(              XK_6,                             5)
	TAGKEYS(              XK_7,                             6)
	TAGKEYS(              XK_8,                             7)
	TAGKEYS(              XK_9,                             8)

	/* TAGMON(               XK_F1,                             0) */
	/* TAGMON(               XK_F2,                             1) */
	/* TAGMON(               XK_F3,                             2) */
	/* TAGMON(               XK_F4,                             3) */

	{ MODKEY|ShiftMask,   XK_Escape,             quit,           {0} },

};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            Mod4Mask,       Button1,        tag,            {0} },
	{ ClkTagBar,            Mod4Mask,       Button3,        toggletag,      {0} },
};

